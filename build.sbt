name := "Supply Zeppelin"

version := "1.0"

scalaVersion := "2.12.2"

resolvers += Resolver.bintrayRepo("dv8fromtheworld", "maven")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.1",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.1",
  "net.dv8tion" % "JDA" % "3.1.0_206",
  "org.hsqldb" % "hsqldb" % "2.4.0",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6"
)
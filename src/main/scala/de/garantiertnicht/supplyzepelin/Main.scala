/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin

import akka.actor.{ActorRef, ActorSystem, Props}
import de.garantiertnicht.supplyzepelin.bot.{Bot, GuildMessage, GuildReaction}
import de.garantiertnicht.supplyzepelin.persistence.definitions._
import de.garantiertnicht.supplyzepelin.persistence.{PersistenceService, Prepare}
import net.dv8tion.jda.core.entities.{Emote, Member}
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.{AccountType, JDA, JDABuilder}

object Main {
  private val system = ActorSystem("supply-zeppelin")
  // Path must be absolute
  var persistence: ActorRef = _
  val bot: ActorRef = system.actorOf(Props[Bot], "bot")
  var reaction: Emote = _
  var jda: JDA = _
  var superUser = -1L
  val MAX_POINTS = 2000000000
  def maxPointsExceededMessage(member: Member) = s"Sorry, but you can not have more or equal then $MAX_POINTS, ${member.getAsMention}"
  val MAX_POINT_MODIFICATION = MAX_POINTS / 2
  def maxPointsModificationExceededMessage(member: Member) = s"Sorry, but you may not modify your points by an amount more or equal then $MAX_POINT_MODIFICATION points, ${member.getAsMention}"

  def main(args: Array[String]): Unit = {
    if(args.length < 3) {
      println("Missing Token, superuser id or jdbc url!")
      sys.exit(2)
    }

    val token = args(0)
    superUser = args(1).toLong
    val db = args.slice(2, args.length).mkString(" ")

    persistence = system.actorOf(Props(classOf[PersistenceService], db, Seq[Prepare](UpdateScore, InsertScore, GetScoreboard, GetPointsForMember, DegradePoints, SetScore, GetBlockedMembers, DeleteZeros)), "persistence")

    jda = new JDABuilder(AccountType.BOT).setToken(token).addEventListener(Events).setEnableShutdownHook(false).buildAsync()
  }
}

object Events extends ListenerAdapter {
  override def onReady(event: ReadyEvent): Unit = {
    Main.reaction = event.getJDA.getEmoteById(318138148242849792L)
  }

  override def onGuildMessageReceived(event: GuildMessageReceivedEvent): Unit = {
    Main.bot ! GuildMessage(event.getMessage)
  }

  override def onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent): Unit = {
    Main.bot ! GuildReaction(event.getReaction, event.getMember)
  }
}

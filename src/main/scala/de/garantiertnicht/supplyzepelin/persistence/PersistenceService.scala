/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence

import java.sql.{Connection, DriverManager}

import akka.actor.Actor
import de.garantiertnicht.supplyzepelin.bot.StopSystem

class PersistenceService(database: String, prepares: Prepare*) extends Actor {
  var connection: Connection = _

  override def preStart(): Unit = {
    if(connection == null || connection.isClosed) {
      org.hsqldb.jdbc.JDBCDriver.driverInstance

      connection = DriverManager.getConnection(database)
      prepares.foreach(_.prepare(connection))
    }
  }

  override def postStop(): Unit = {
    if(connection != null && !connection.isClosed) {
      connection.close()
      connection = null
    }
  }

  override def receive: Receive = {
    case custom: CustomStatement =>
      try {
        custom.prepare(connection)
        sender ! custom.sql.executeUpdate()
      } catch {
        case _: Exception => sender ! -1
      }
    case custom: CustomQuery =>
      try {
        custom.prepare(connection)
        val result = custom.sql.executeQuery()
        sender ! custom.process(result)
      } catch {
        case e: Exception => sender ! e.getMessage
      }
    case persistable: PersistenceStatement =>
      var statement: Option[PersistenceStatement] = Some(persistable)

      while(statement.nonEmpty) {
        val currentStatement = statement.get
        val updatedRowCount = currentStatement.sql.executeUpdate()
        statement = currentStatement.process(updatedRowCount)
      }
    case query: PersistenceQuery[_] =>
      var statement: Option[PersistenceQuery[_]] = Some(query)

      while(statement.nonEmpty) {
        val currentStatement = statement.get

        val result = currentStatement.sql.executeQuery()
        statement = currentStatement.preProcess(result.getFetchSize)

        if(statement.isEmpty) {
          sender ! currentStatement.process(result)
        }
      }
    case StopSystem => connection.close()
  }
}

/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import java.sql.{Connection, PreparedStatement}

import de.garantiertnicht.supplyzepelin.persistence.{PersistenceStatement, Prepare}
import net.dv8tion.jda.core.entities.Member

case class UpdateScore(member: Member, addition: Int) extends PersistenceStatement {
  private var statement: PreparedStatement = _

  override def sql: PreparedStatement = {
    UpdateScore.statement.setInt(1, addition)
    UpdateScore.statement.setLong(2, member.getGuild.getIdLong)
    UpdateScore.statement.setLong(3, member.getUser.getIdLong)

    UpdateScore.statement
  }

  override def process(rowCount: Int): Option[PersistenceStatement] = {
    if(rowCount == 0) {
      return Some(InsertScore(member, addition))
    }

    None
  }
}

object UpdateScore extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      s"UPDATE SCOREBOARD SET SCORE = SCORE + ? WHERE GUILDID = ? AND USERID = ?"
    )
  }
}

// SET

case class SetScore(member: Member, addition: Int) extends PersistenceStatement {
  private var statement: PreparedStatement = _

  override def sql: PreparedStatement = {
    SetScore.statement.setInt(1, addition)
    SetScore.statement.setLong(2, member.getGuild.getIdLong)
    SetScore.statement.setLong(3, member.getUser.getIdLong)

    SetScore.statement
  }

  override def process(rowCount: Int): Option[PersistenceStatement] = {
    if(rowCount == 0) {
      return Some(InsertScore(member, addition))
    }

    None
  }
}

object SetScore extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      "UPDATE SCOREBOARD SET SCORE = ? WHERE GUILDID = ? AND USERID = ?"
    )
  }
}

// INSERT


case class InsertScore(member: Member, addition: Int) extends PersistenceStatement {
  private var statement: PreparedStatement = _

  override def sql: PreparedStatement = {
    InsertScore.statement.setLong(1, member.getGuild.getIdLong)
    InsertScore.statement.setLong(2, member.getUser.getIdLong)
    InsertScore.statement.setInt(3, addition)

    InsertScore.statement
  }
}

object InsertScore extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      "INSERT INTO SCOREBOARD VALUES (?, ?, ?)"
    )
  }
}

/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import java.sql.{Connection, PreparedStatement, ResultSet}

import de.garantiertnicht.supplyzepelin.persistence.{PersistenceQuery, Prepare}
import net.dv8tion.jda.core.entities.Guild

import scala.collection.mutable.ListBuffer

class GetBlockedMembers(guild: Guild) extends PersistenceQuery[Seq[Long]] {
  override def process(results: ResultSet): Seq[Long] = {
    var place = 1
    val ids = ListBuffer[Long]()

    while(results.next()) {
      ids += results.getLong(1)
    }

    ids
  }

  override def sql: PreparedStatement = {
    GetBlockedMembers.statement.setLong(1, guild.getIdLong)
    GetBlockedMembers.statement
  }
}

object GetBlockedMembers extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      s"SELECT USERID FROM SCOREBOARD WHERE GUILDID = ? AND SCORE = ${Int.MinValue}"
    )
  }
}


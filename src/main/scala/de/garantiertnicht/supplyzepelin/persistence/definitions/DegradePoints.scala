/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import java.sql.{Connection, PreparedStatement}

import de.garantiertnicht.supplyzepelin.persistence.{PersistenceStatement, Prepare}

object DegradePoints extends PersistenceStatement with Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(s"UPDATE SCOREBOARD SET SCORE = SCORE * 0.995 WHERE SCORE > ${Int.MinValue}")
  }

  override def sql: PreparedStatement = statement

  override def process(rowCount: Int): Option[PersistenceStatement] = Some(DeleteZeros)
}

object DeleteZeros extends PersistenceStatement with Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement("DELETE FROM SCOREBOARD WHERE SCORE = 0")
  }

  override def sql: PreparedStatement = statement
}

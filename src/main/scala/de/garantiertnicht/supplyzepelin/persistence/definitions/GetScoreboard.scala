/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import java.sql.{Connection, PreparedStatement, ResultSet}

import de.garantiertnicht.supplyzepelin.persistence.{PersistenceQuery, Prepare}
import net.dv8tion.jda.core.entities.{Guild, Member}

import scala.collection.mutable.ListBuffer

case class ScoreboardEntry(member: Option[Member], score: Int)

class GetScoreboard(guild: Guild, page: Int) extends PersistenceQuery[List[ScoreboardEntry]] {
  private var statement: PreparedStatement = _

  override def sql: PreparedStatement = {
    GetScoreboard.statement.setLong(1, guild.getIdLong)
    GetScoreboard.statement.setInt(2, page * 15)
    GetScoreboard.statement
  }

  override def process(results: ResultSet): List[ScoreboardEntry] = {
    val list = new ListBuffer[ScoreboardEntry]

    while(results.next()) {
      val memberId = results.getLong(1)
      val score = results.getInt(2)

      val user = guild.getMemberById(memberId)
      val entry = ScoreboardEntry(Option.apply(user), score)

      list += entry
    }

    list.toList
  }
}

object GetScoreboard extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      s"SELECT USERID, SCORE FROM SCOREBOARD WHERE GUILDID = ? AND SCORE > ${Int.MinValue} ORDER BY SCORE DESC OFFSET ? FETCH NEXT 15 ROWS ONLY"
    )
  }
}

/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import java.sql.{Connection, PreparedStatement, ResultSet}

import de.garantiertnicht.supplyzepelin.persistence.{PersistenceQuery, Prepare}
import net.dv8tion.jda.core.entities.Member

case class RankPosition(score: Int, position: Int)

class GetPointsForMember(member: Member) extends PersistenceQuery[RankPosition] {
  override def process(results: ResultSet): RankPosition = {
    var place = 1
    while(results.next()) {
      val memberId = results.getLong(1)
      if(memberId == member.getUser.getIdLong) {
        return RankPosition(results.getInt(2), place)
      }

      place += 1
    }

    RankPosition(0, place)
  }

  override def sql: PreparedStatement = {
    GetPointsForMember.statement.setLong(1, member.getGuild.getIdLong)
    GetPointsForMember.statement
  }
}

object GetPointsForMember extends Prepare {
  var statement: PreparedStatement = _

  override def prepare(connection: Connection): Unit = {
    statement = connection.prepareStatement(
      "SELECT USERID, SCORE FROM SCOREBOARD WHERE GUILDID = ? ORDER BY SCORE DESC"
    )
  }
}

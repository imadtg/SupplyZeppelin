/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence

import java.sql.{Connection, PreparedStatement, ResultSet}

abstract class Persistence {
  def sql: PreparedStatement
}

abstract class PersistenceStatement extends Persistence {
  def process(rowCount: Int): Option[PersistenceStatement] = None
}

abstract class PersistenceQuery[T] extends Persistence {
  def preProcess(resultCount: Int): Option[PersistenceQuery[T]] = None
  def process(results: ResultSet): T
}

case class CustomQuery(sqlString: String) extends PersistenceQuery[String] with Prepare {
  private var statement: PreparedStatement = _
  override def prepare(connection: Connection): Unit = statement = connection.prepareStatement(sqlString)
  override def sql: PreparedStatement = statement
  override def process(results: ResultSet): String = {
    val rows = results.getMetaData.getColumnCount
    val builder: StringBuilder = new StringBuilder
    while(results.next()) {
      for(i <- 1 to rows) {
        builder.append(results.getString(i))
        builder.append(';')
      }

      builder.replace(builder.length - 1, builder.length, "\n")
    }

    builder.mkString
  }
}

case class CustomStatement(sqlString: String) extends PersistenceStatement with Prepare {
  private var statement: PreparedStatement = _
  override def prepare(connection: Connection): Unit = statement = connection.prepareStatement(sqlString)
  override def sql: PreparedStatement = statement
}

trait Prepare {
  def prepare(connection: Connection)
}
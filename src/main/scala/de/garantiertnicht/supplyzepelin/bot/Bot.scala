/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import akka.actor.SupervisorStrategy.{Resume, Stop}
import akka.actor.{Actor, ActorInitializationException, ActorKilledException, ActorRef, AllForOneStrategy, DeathPactException, Props, SupervisorStrategy}
import akka.event.Logging
import akka.routing.{DefaultResizer, SmallestMailboxPool}
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.{CommandExecution, CommandManager}
import de.garantiertnicht.supplyzepelin.persistence.definitions.DegradePoints
import net.dv8tion.jda.core.entities.{Guild, Member, Message, MessageReaction}

import scala.collection.mutable
import scala.concurrent.duration._

case class GuildActorPoisoned(guild: Guild)
case class GuildCommand(message: Message, command: String)
case class GuildMessage(message: Message)
case class GuildBotMessage(message: Message)
case class GuildReaction(reaction: MessageReaction, author: Member)
object StopSystem

class Bot extends Actor {
  val BIG_MASTER_SWITCH = false

  val map: mutable.Map[String, ActorRef] = new mutable.HashMap[String, ActorRef]
  val prefix = ";"
  val log = Logging(context.system, this)

  val commandResizer = DefaultResizer(lowerBound = 1, upperBound = 5)
  val commandDispatcher = context.actorOf(SmallestMailboxPool(1, Some(commandResizer)).props(Props(classOf[CommandManager], Seq
  (
    command.documentation.Help,
    command.fun.Bribe,
    command.management.Block,
    command.management.Unblock,
    command.points.Points,
    command.points.Scoreboard,
    command.points.Transfer,
    command.system.Stop,
    command.system.SqlQuery,
    command.system.SqlStatement,
    command.system.DebugInfo,
    command.system.Stats
  ))), "command-dispatcher")

  var shouldStop = false

  implicit val dispatcher = context.system.dispatcher

  override def preStart(): Unit = {
    context.system.scheduler.schedule(1 hour, 90 minutes, self, DegradePoints)
  }

  override def postStop(): Unit = {
    map.clear()
  }

  def getGuildActor(guild: Guild): ActorRef = {
    val option = map.get(guild.getId)
    if(option.nonEmpty) {
      return option.get
    }

    if(!shouldStop && (!BIG_MASTER_SWITCH || guild.getIdLong == 204155906638872576L)) {
      try {
        val props = Props(classOf[GuildActor], guild)
        val actor = context.actorOf(props, s"${guild.getId}-${guild.getName.filter(character ⇒ character.isLetterOrDigit && character < 128)}")
        map += ((guild.getId, actor))

        log.debug(s"Guild ${guild.getName} (${guild.getId}) was loaded.")
      } catch {
        case e: Exception =>
          log.warning(s"Guild ${guild.getName} (${guild.getId}) failed to load:")
          e.printStackTrace()
      }
    }

    context.system.deadLetters
  }

  override def receive: Receive = {
    case isPoisoned: GuildActorPoisoned =>
      map.remove(isPoisoned.guild.getId)
      log.debug(s"Guild ${isPoisoned.guild.getName} (${isPoisoned.guild.getId}) was unloaded.")
      if(shouldStop && map.isEmpty) {
        log.info("All guilds unloaded, waiting for queued actions")
        Main.persistence ! StopSystem
        Thread.sleep(15000)
        Main.jda.shutdown()
        context.system.terminate().onComplete(sys.exit())
      }
    case message: GuildMessage =>
      if(!message.message.getAuthor.isBot) {
        val content = message.message.getRawContent
        val guildActor = getGuildActor(message.message.getGuild)
        if(content.startsWith(prefix)) {
          map.get(message.message.getGuild.getId).foreach(_ ! GuildCommand(message.message, content.substring(prefix.length)))
        } else if(content.nonEmpty && content.charAt(0).isLetter) {
          map.get(message.message.getGuild.getId).foreach(_ forward message)
        }
      } else {
        getGuildActor(message.message.getGuild) ! GuildBotMessage(message.message)
      }
    case reaction: GuildReaction =>
      if(!reaction.author.getUser.isBot) {
        map.get(reaction.reaction.getGuild.getId).foreach(_ forward reaction)
      }
    case DegradePoints =>
      Main.persistence ! DegradePoints
    case StopSystem =>
      log.info("Shutting down…")
      shouldStop = true
      map.foreach(_._2 ! GuildActorShouldBePoisoned)
    case command: CommandExecution => commandDispatcher ! command
  }

  override def supervisorStrategy: SupervisorStrategy = AllForOneStrategy(maxNrOfRetries = 3, withinTimeRange = 10 minutes) {
    case _: ActorInitializationException => Stop
    case _: ActorKilledException         => Stop
    case _: DeathPactException           => Stop
    case _: Exception                    ⇒ Resume
  }
}

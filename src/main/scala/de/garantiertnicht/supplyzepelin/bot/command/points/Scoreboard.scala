/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.points

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.{GetScoreboard, ScoreboardEntry}
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Scoreboard extends Command {
  override val name: String = "scoreboard"
  implicit val timeout = Timeout(1 minute)


  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    val page = if (commandAndArgs.size > 1) {
      val pageString = commandAndArgs(1)

      try {
        math.max(pageString.toInt - 1, 0)
      } catch {
        case numberFormat: NumberFormatException => 0
      }
    } else {
      0
    }

    val entries = Main.persistence ? new GetScoreboard(originalMessage.getGuild, page)
    var place = page * 15 + 1
    entries.onSuccess({
      case result: Seq[ScoreboardEntry] =>
        val builder = result.map(entry => if (entry.member.nonEmpty) {
          s"${entry.member.get.getAsMention} with ${entry.score} points"
        } else {
          s"discordUser with ${entry.score} points"
        }).foldLeft(new StringBuilder)((cat, in) => {
          cat.append(place)
          place += 1
          cat.append(". ")

          cat.append(in)
          cat.append('\n')
        })

        val string = if (builder.isEmpty) {
          "Nothing here yet o.0"
        } else {
          builder.substring(0, builder.length - 1)
        }

        val embed = new EmbedBuilder().setDescription(string).setTitle(s"Scoreboard for ${originalMessage.getGuild.getName} on page ${page + 1}").setFooter("Points will decay slowly with a rate of 0.5 % every 90 minutes ;-)", null).build()
        originalMessage.getChannel.sendMessage(embed).queue()
    })
  }
}

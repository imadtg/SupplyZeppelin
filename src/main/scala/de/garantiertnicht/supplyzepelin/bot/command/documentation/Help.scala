/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.documentation

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.command.Command
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.{Guild, Message}

import scala.concurrent.ExecutionContext

object Help extends Command {
  override val name: String = "help"
  val links = List(
    ("Check if the Large Hadron Collider has ended the world yet", "http://hasthelargehadroncolliderdestroyedtheworldyet.com/"),
    ("All your points are belong to us", "https://youtu.be/8fvTxv46ano"),
    ("Click here for free points!", "https://youtu.be/DLzxrzFCyOs"),
    ("Get some healthy stuff", "https://youtu.be/Oiu_OfCPy4U"),
    ("Look at this cute kitten!", "https://youtu.be/EyFObnbB1dw"),
    ("Click here for a random color", "http://randomcolour.com"),
    ("Check the progress on the great wall of North America", "http://wheresthewalldonald.com")
  )

  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    val embed = new EmbedBuilder()
      .setTitle("No worries, I'm here to help you!")
      .setDescription("Hello there! I will randomly drop crates in chat when its active. Be the first to react to my message and you will get them!")
      .addField(";points [@user]", "Check the points you or another user has made so far!", false)
      .addField(";scoreboard [page]", "Check the top members of the server!", false)
      .addField(";bribe <amount>", "You can gamble your points to get more! If you collect the next crate, you will get twice the amount you bribed in addition to the crates value, else the collector gets one third (and you nothing)", false)
      .addField(";transfer <amount> <mentions>…", "Transfer the amount to each mentioned user. Transfer fees apply.", false)
      .addField(";help", "Calculate the distance between two parallel universes", false)
      .addField(";block <@user>", "Blocks a user from using this bot on your guild.", false)
      .addField(";unblock <@user>", "Sets the users score to 0 and unblocks the user if it was blocked.", false)
      .addField("Links", "[Developers Guild (Support)](https://discord.gg/ymFAdkG)\n" +
        "[Add me to your guild!](https://discordapp.com/oauth2/authorize?client_id=317248850672746496&scope=bot&permissions=355392)\n" +
        "[See my source code](https://gitlab.com/garantiertnicht/SupplyZeppelin)\n" +
        getLink(originalMessage.getGuild), false)
      .build()

    originalMessage.getChannel.sendMessage(embed).queue()
  }

  def getLink(guild: Guild): String = {
    val linkVal = (guild.getIdLong % links.size).toInt
    val link = links(linkVal)

    s"[${link._1}](${link._2})"
  }
}

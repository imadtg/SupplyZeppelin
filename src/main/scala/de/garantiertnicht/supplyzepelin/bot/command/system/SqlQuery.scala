/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.system

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.CustomQuery
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object SqlQuery extends Command {
  implicit val timeout = Timeout(1 minute)
  override val name: String = "sqlquery"

  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    if(originalMessage.getAuthor.getIdLong == Main.superUser) {
      val sql = commandAndArgs.mkString(" ").substring(commandAndArgs(0).length)
      val result = Main.persistence ? CustomQuery(sql)
      result.onSuccess {
        case string: String => originalMessage.getChannel.sendMessage(string.substring(0, math.min(2000, string.length))).queue()
      }
    }
  }
}
